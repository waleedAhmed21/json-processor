package json.entity;

import java.util.List;


/***
 * 
 * @author Waleed
 *
 */
public class FeatureConfig {
	private int id;
	private String name;
	private List<Transform> transforms;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Transform> getTransforms() {
		return transforms;
	}
	public void setTransforms(List<Transform> transforms) {
		this.transforms = transforms;
	}

}
