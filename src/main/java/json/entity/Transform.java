package json.entity;

import com.fasterxml.jackson.databind.node.ObjectNode;

/***
 * 
 * @author Waleed
 *
 */
public class Transform {
	private String name;
	private boolean useInML = true;
	private boolean enabled = true;
	private String jsltExpression;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isUseInML() {
		return useInML;
	}
	public void setUseInML(boolean useInML) {
		this.useInML = useInML;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getJsltExpression() {
		return jsltExpression;
	}
	public void setJsltExpression(String jsltExpression) {
		this.jsltExpression = jsltExpression;
	}
	
	public TransformNode getTransformNode() {
		return new TransformNode(getName(), getJsltExpression());
	}

}
