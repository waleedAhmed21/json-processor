package json.entity;

import com.fasterxml.jackson.databind.JsonNode;

/***
 * 
 * @author Waleed
 *
 */
public class FeatureJsonPayLoadWrapper {
	private FeatureConfig featureConfig;
	private JsonNode payloadJson;
	
	public FeatureConfig getFeatureConfig() {
		return featureConfig;
	}
	public void setFeatureConfig(FeatureConfig featureConfig) {
		this.featureConfig = featureConfig;
	}
	public JsonNode getPayloadJson() {
		return payloadJson;
	}
	public void setPayloadJson(JsonNode payloadJson) {
		this.payloadJson = payloadJson;
	}
	
	

}
