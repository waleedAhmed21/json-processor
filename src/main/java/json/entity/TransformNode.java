package json.entity;
/***
 * 
 * @author Waleed
 *
 */
public class TransformNode {
	private String name;
	private String jsltExpression;
	
	public TransformNode(String name, String jsltExpression) {
		this.name = name;
		this.jsltExpression = jsltExpression;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJsltExpression() {
		return jsltExpression;
	}

	public void setJsltExpression(String jsltExpression) {
		this.jsltExpression = jsltExpression;
	}
	
	
}
