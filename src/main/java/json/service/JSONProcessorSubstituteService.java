package json.service;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;

import json.entity.FeatureConfig;
import json.processor.JSONProcessor;

/***
 * 
 * @author Waleed
 *
 */
@Service
public class JSONProcessorSubstituteService {
	
	/***
	 * this method gets called from the controller and just routes the call to the JSON processor
	 * which acts like a visitor doing the job in stateless manner
	 * @param featureConfig: the featureConfig object   
	 * @param inputJSON: the JsonNode representing the free form json input
	 * @return
	 */
	public String retrieveFeatures( FeatureConfig featureConfig, JsonNode inputJSON) {
		return new JSONProcessor().process(featureConfig, inputJSON);
	}

}
