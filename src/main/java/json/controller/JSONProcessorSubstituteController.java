package json.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import json.entity.FeatureConfig;
import json.entity.FeatureJsonPayLoadWrapper;
import json.service.JSONProcessorSubstituteService;

/**
 *
 * @author Waleed
 *
 */

@RestController
@Validated
public class JSONProcessorSubstituteController {

  private final JSONProcessorSubstituteService jsonProcessorSubstituteService;

  @Autowired
  public JSONProcessorSubstituteController(final JSONProcessorSubstituteService jsonProcessorSubstituteService) {
    Assert.notNull(jsonProcessorSubstituteService, "service can't be null");
    this.jsonProcessorSubstituteService = jsonProcessorSubstituteService;
  }

  /***
   * The only rest endpoint here 
   * @param featureJsonPayLoadWrapper a wrapper object that contains the featureConfig object and 
   * a payload json that can change its format
   * a sample input would look like this 
			   * {
			  "featureConfig":{
			    "id": 1,
			    "name": "DeviceFeatures",
			    "transforms": [
			        {
			            "name": "device_os",
			            "useInML": true,
			            "enabled": true,
			            "jsltExpression": ".device.osType"
			        },
			        {
			            "name": "device_description",
			            "useInML": true,
			            "enabled": true,
			            "jsltExpression": ".device.osType + \" \" + .device.model"
			        }
			    ]
			},
			  "payloadJson":{
			    "eventId": "878237843",
			    "device": {
			          "osType": "Linux",
			          "model": "Laptop"
			         },
			    "ip" : "10.45.2.30",
			    "sessionId": "ads79uoijd098098"
			  }
			} 
   * @return a response entity containing the output json string after substitution with jslt enabled expressions
   */
  
  @RequestMapping(value = "/retrieveFeatures", method = RequestMethod.POST)
  public ResponseEntity<String> retrieveFeatures(
      @RequestBody FeatureJsonPayLoadWrapper featureJsonPayLoadWrapper){
    String outputJSON = jsonProcessorSubstituteService.retrieveFeatures(featureJsonPayLoadWrapper.getFeatureConfig(), 
    		featureJsonPayLoadWrapper.getPayloadJson());
    if (outputJSON == null) {
    	return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(outputJSON, HttpStatus.OK);
  }
  
}
