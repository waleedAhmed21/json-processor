package json;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 
 * @author Waleed
 *
 */

@SpringBootApplication
@ComponentScan({"json"})
public class JSONProcessorSubstituteApplication {

	public static void main(String[] args) {
		SpringApplication.run(JSONProcessorSubstituteApplication.class, args);
	}

}
