package json.processor;

import json.constants.Constants;
import json.entity.FeatureConfig;
import json.entity.Transform;
import json.entity.TransformNode;

import com.schibsted.spt.data.jslt.Parser;
import com.schibsted.spt.data.jslt.impl.NodeUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.logging.log4j2.Log4J2LoggingSystem;
import org.springframework.boot.logging.logback.LogbackLoggingSystem;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.schibsted.spt.data.jslt.Expression;

/***
 * 
 * @author Waleed
 *
 */
public class JSONProcessor {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	/***
	 * this is the main method in this class, and the main functionaity here is to process the inputjson
	 * according to the given featureConfig object
	 * the algorithm is as follows:
	 * 1. First calls getEnabledTransforms method to filter out the disabled transforms, 
	 *     and returns a list of TransformNode which contains the name and jsltExpression,
	 * 2. The jslt expression is then constructed via the  constructJSLTExpression method,
	 * 3. finally the actual substitute method is calling which returns the final output      
	 * @param featureConfig
	 * @param inputJsonRoot
	 * @return
	 */
	public String process( FeatureConfig featureConfig, JsonNode inputJsonRoot) {
		String outputJsonText = null;
		try {            
			List<TransformNode> transforms = getEnabledTransforms(featureConfig.getTransforms());
			String jslt = constructJSLTExpression(inputJsonRoot, transforms);
			 outputJsonText = substitute(inputJsonRoot, jslt);
		} catch (Exception e) {
			log.info(e.getMessage(), e);
			return null;
		}
		return outputJsonText;
	}
	
	/***
	 * This method constructs the whole jslt expression from the list of enabled transformNodes,
	 * the reason behind all this is simply to come up with one unified jslt expression 
	 * because otherwise we would loop through the list of transfors and feed the parser one by one and the parser will 
	 * loop thrugh all the inout json for each one to check for matches and does the substitution and then
	 * the running time would be very bad for long input jsons but that way, we feed one accumulative input
	 * and there should be one loop in the Parser.compileString call
	 * @param inputJsonRoot
	 * @param transforms
	 * @return
	 */
	private String constructJSLTExpression(JsonNode inputJsonRoot, List<TransformNode> transforms) {
		ObjectMapper objectMapper = new ObjectMapper();
		Map inputMap = objectMapper.convertValue(inputJsonRoot, Map.class);
		Map jsltMap = new HashMap<String, String>();
		if (inputMap.containsKey(Constants.INPUT_JSON_KEYWORD)) {
			jsltMap.put(Constants.INPUT_JSON_KEYWORD, inputMap.get(Constants.INPUT_JSON_KEYWORD));
		}
		for (TransformNode transformNode:transforms) {
			jsltMap.put(transformNode.getName(), transformNode.getJsltExpression());
		}       
		JsonNode jsltNode = objectMapper.convertValue(jsltMap, JsonNode.class);
		return removeQuotesFromExpressions(jsltNode.toPrettyString());
	}
	
	/***
	 * This method simply uses a few regEx to replace all not needed quotes which would ruin the compileString call later 
	 * @param jsonInput
	 * @return
	 */
	private String removeQuotesFromExpressions(String jsonInput) {
		jsonInput = jsonInput.replaceAll(": \"\\.", ":\\.");
		jsonInput = jsonInput.replaceAll("\",", ",");
		jsonInput = jsonInput.replaceFirst(",", "\",");
		jsonInput = jsonInput.replace(jsonInput.substring(jsonInput.lastIndexOf("\"")),jsonInput.substring(jsonInput.lastIndexOf("\"") + 1));
		jsonInput = jsonInput.replaceAll("\\\\", "");
		return jsonInput;
	}
	
	/***
	 * This is the method that actually does the parse and substitute, and return with the output 
	 * @param input
	 * @param transformJSLT
	 * @return
	 * @throws JsonProcessingException
	 */
	private String substitute(JsonNode input, String transformJSLT) throws JsonProcessingException {
		JsonNode outputNode = null;
		try {
			Expression jslt = Parser.compileString(transformJSLT);
			outputNode = jslt.apply(input);
		}
		catch (Exception e) {
			log.info(e.getMessage(), e);
			return null;
		}
		
		return NodeUtils.mapper.writerWithDefaultPrettyPrinter().writeValueAsString(outputNode);
	}
	
	
	/***
	 * A simple lambda expression to filter out the disabled transforms and aggreagte the required list of TransformNodes
	 * @param transforms
	 * @return
	 */
	private List<TransformNode> getEnabledTransforms(List<Transform> transforms){
		List<TransformNode> enabledTransforms = transforms
				  .parallelStream()
				  .filter(transform -> transform.isEnabled())
				  .map(Transform::getTransformNode)
			      .collect(Collectors.toList());
		return enabledTransforms;
	}
}
