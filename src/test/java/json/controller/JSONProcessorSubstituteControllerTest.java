package json.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import json.entity.FeatureConfig;
import json.entity.FeatureJsonPayLoadWrapper;
import json.entity.Transform;
import json.service.JSONProcessorSubstituteService;

/***
 * 
 * @author Waleed
 *
 */

@ExtendWith(MockitoExtension.class)
public class JSONProcessorSubstituteControllerTest {
	@InjectMocks
	JSONProcessorSubstituteController jsonProcessorSubstituteController;
	
	@Mock
	JSONProcessorSubstituteService jsonProcessorSubstituteService;
	String response;
	FeatureConfig correctFeatureConfig;
	String inputJson;
	JsonNode inputJsonNode;
	String inCorrectInpuJSON;
	FeatureJsonPayLoadWrapper featureJsonPayLoadWrapper;
	
	@BeforeEach
	public void setup() {
		Transform transform1 = new Transform();
		transform1.setName("device_os");
		transform1.setEnabled(true);
		transform1.setUseInML(true);
		transform1.setJsltExpression(".device.osType");
		
		Transform transform2 = new Transform();
		transform2.setName("device_description");
		transform2.setEnabled(true);
		transform2.setUseInML(true);
		transform2.setJsltExpression(".device.osType + \\\" \\\" + .device.model");
		
		List<Transform> transforms= new ArrayList<>();
		transforms.add(transform1);
		transforms.add(transform2);
		
		correctFeatureConfig = new FeatureConfig();
		correctFeatureConfig.setId(1);
		correctFeatureConfig.setName("DeviceFeatures");
		correctFeatureConfig.setTransforms(transforms);
		
		inputJson = "\"eventId\": \"878237843\",\r\n"
				+ "    \"device\": {\r\n"
				+ "          \"osType\": \"Linux\",\r\n"
				+ "          \"model\": \"Laptop\"\r\n"
				+ "         },\r\n"
				+ "    \"ip\" : \"10.45.2.30\",\r\n"
				+ "    \"sessionId\": \"ads79uoijd098098\"\r\n"
				+ "  }";
		
		inputJsonNode = (new ObjectMapper()).convertValue(inputJson, JsonNode.class);
		
		featureJsonPayLoadWrapper.setFeatureConfig(correctFeatureConfig);
		featureJsonPayLoadWrapper.setPayloadJson(inputJsonNode);
	}
	
	@Test
	public void testRetrieveFeaturesAllEnabled() {
		response = "{\r\n"
				+ "  \"eventId\" : \"878237843\",\r\n"
				+ "  \"device_description\" : \"Linux Laptop\",\r\n"
				+ "  \"device_os\" : \"Linux\"\r\n"
				+ "}";
		ResponseEntity<String> responseEntity  = jsonProcessorSubstituteController.retrieveFeatures(featureJsonPayLoadWrapper);
	    assertEquals(response, responseEntity.getBody());

	}
}
